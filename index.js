'use strict';
const { readFileSync } = require('fs');
const { test, extract } = require('../exp');

const TYPE = (validate, parse, empty) => {
	return {
		validate,
		parse,
		empty
	};
};
const EXTENSION = (labels, hook) => {
	return {
		labels,
		// Right, this is where we were. Time to get at it. You did well focusing today. Keep it up. You've got an interesting night ahead.
		// Finsih init stuff by 9. After that we build our simple web server project with node.
		// Don't worry about Z. Build an application that allows me to set places on a map, and another tab that allows me to track
		// my movement and shit. C'mon. It's a hackathon if I ever saw one.
	};
};
const DESCRIPTOR = (type = 'string', def = null, required = false) => {
	return {
		type,
		default: def,
		required
	};
};
const EXP_VALIDATOR = expression => text => test(expression, text);
const TRUE = () => true;
const FALSE = () => false;
const NULL = () => null;
const TRUE_STR = x => x === "true";
const DO_NOTHING = x => x;

const TYPES = {
	int: TYPE(
		EXP_VALIDATOR(/^(\+|-)?[0-9]+$/),
		parseInt,
		() => 0
	),
	float: TYPE(
		EXP_VALIDATOR(/^(\+|-)?[0-9]+\.([0-9]+)?$/),
		parseFloat,
		() => 0.0
	),
	bool: TYPE(
		EXP_VALIDATOR(/^(true|false)$/i),
		TRUE_STR,
		() => false
	),
	string: TYPE(
		TRUE,
		DO_NOTHING,
		() => ""
	),
	null: TYPE(
		FALSE,
		DO_NOTHING,
		NULL
	)
};

const TEXT = 'utf8';
const PARAMS = '.params';
const ARGS = '.args';
const INI = '.ini';
const POINT = '.';
const COMMENT = '#';
const DECLARE = ':';
const ASSIGN = '=';
const LBLOCK = '[';
const NEWLINE = '\n';
const HEADLINE_FORMAT = /\[\s?(\w+)\s?\]/;
const PARSE_LINE = (symbol, operation, line, [key, str] = line.split(symbol)) => [key.trim(), operation(str.trim())];
const PARSE_BLOCK = (headline, lines, result = extract(HEADLINE_FORMAT, headline)) => [result ? result[1] : null, parse(lines || [])];

// TODO: How can we macro something like this?
const OPERATORS = {
	[DECLARE]: line => PARSE_LINE(DECLARE, declare, line),
	[ASSIGN]: line => PARSE_LINE(ASSIGN, typify, line),
	[LBLOCK]: (headline, index, lines, pivot = index + 1) => {
		let reste = lines.slice(pivot);
		let ind = reste.findIndex(line => test(HEADLINE_FORMAT, line));
		return PARSE_BLOCK(headline, lines.splice(pivot, ind > -1 ? ind : reste.length));
	},
};

const EXTENSIONS = {
	[PARAMS]: () => {

	},
	[ARGS]: () => {

	},
	[INI]: () => {

	},
};

const o = (input, output = {...(input || {})}) => output;
const keys = x => Object.keys(x);
const values = x => Object.values(x);
const entries = x => Object.entries(x);
const truncate = (line, delim) => line.substring(0, Math.max(0, line.indexOf(delim)) || undefined).trim();
const get_type = name => TYPES[name] || TYPES.null;
const not_null = val => val !== null;
const match_to_type = str => values(TYPES).find(type => type.validate(str));
const typify = str => match_to_type(str).parse(str);
const declare = type => get_type(type).empty();
const objectify = (map, output = o()) => map.forEach(([key, value]) => output[key] = value) || output;

const parse = lines => objectify(lines
	// TODO: move some of these maps and filters into "prepare" function
	.map(line => line.trim()) // TRIM OOR LINES
	.filter(line => !!line) // EMPTY LINES
	.filter(line => line.charAt(0) !== COMMENT) // COMMENTS
	.map(line => truncate(line, COMMENT)) // END OF LINE COMMENTS
	// At this point, we go through the operators and return a key-value pair.
	.map((line, index, lines) => OPERATORS[
			keys(OPERATORS).find(symbol => line.includes(symbol))
		](line, index, lines))
	// <-- perhaps above, I use some sort of callback. This way, our params vs args vs default can hook-in appropriately.... hmmm!
	.filter(([key, val]) => not_null(key) && not_null(val))); // Remove any lines that had errors, yo

const open = file => parse(readFileSync(file, TEXT).split(NEWLINE));

module.exports = {
	parse: text => parse(text.split(NEWLINE)),
	open: file => {
		// Here, we must do some things based on the file extension: .params, .args, .ini/.init/.conf/.cfg/.config
		let ext = file.substring(Math.max(0, file.lastIndexOf(POINT)));
		// Ok cool. so default does a regular parse... we'll strap that up...
		// Then, for the other two:
		// -- params: return a "descriptor" object
		// -- args: attempts to validate the values in the file against the params descriptor and construct a final object.
		// It's easy to do things like Object.assign... ours won't be QUITE that simple.
		// 		But it'll be the final step after going through the descriptor to build the base object.
		return open(file);
	},
	params: file => {
		let params = open(file);
		// 
		return {
			create(args) {
				return {
					...,
					...args
				}
			},

		}
	},
};
